### TO RUN THE APP IN LOCAL
*    Update the `${PORT}` in application.properties file to `8080`.
*    `$ ./gradlew bootjar`
*    `$ ./gradlew bootRun`

### ENDPOINTS
* GET /books
>  This endpoint lists the books

* GET /humans.txt
> This endpoint returns list of contributors

* GET /healthcheck
> Application healthcheck
